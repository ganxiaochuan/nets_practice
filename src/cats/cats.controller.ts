import { Controller, Get, Post, Req, Query, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';
import { CreateCatDto } from './dto/cat.dto';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}
  @Get()
  test(@Query('version') version): object {
    return {
      ...{
        text: `hello world`
      }
    }
  }

  @Get('version')
  async get(@Query('version') version: string): Promise<object> {
    return {
      ...{
        text: `hello world`,
        type: `get`,
        value: version
      }
    }
  }

  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  @Post('create')
  async create(@Body() createCatDto: CreateCatDto) {
    this.catsService.create(createCatDto)
    return {
      data: createCatDto,
      msg: '创建成功'
    }
  }

  @Post('findAll')
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll()
  }
}